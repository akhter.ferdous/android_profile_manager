package com.color.ferdous.profilemanager;

/**
 * Created by ferdous on 8/20/17.
 */

import android.content.Context;
import android.media.AudioManager;
import android.util.Log;



public class ProfileManager {

    private AudioManager audManager;
    Context myContext;

    public ProfileManager(Context myContext) {
        this.myContext = myContext;
        audManager = (AudioManager)myContext.getSystemService(Context.AUDIO_SERVICE);

    }

    public void home() {
        if(audManager.getRingerMode()!= AudioManager.RINGER_MODE_NORMAL)
            audManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        Log.d("profile", "home");
        // Toast.makeText(myContext,"Normal Mode",Toast.LENGTH_SHORT).show();
    }

    public void pocket() {
        if(audManager.getRingerMode()!= AudioManager.RINGER_MODE_VIBRATE)
            audManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
        Log.d("profile", "pocket");
        //Toast.makeText(myContext,"Vibrate Mode",Toast.LENGTH_SHORT).show();
    }

    public void silent() {
        if(audManager.getRingerMode()!= AudioManager.RINGER_MODE_SILENT)
            audManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        Log.d("profile", "silent");
        //Toast.makeText(myContext,"Silent Mode",Toast.LENGTH_SHORT).show();
    }
}
